public class ScheduleCountryUpdate implements Schedulable{
    Public void Execute(SchedulableContext SC){
        List<Opportunity> olist =[Select ID, Name, Country_of__c From Opportunity];
        For(Opportunity o : olist){
            o.Country_of__c = 'USA';
        }
        update olist;
    }

}